package main

import (
	"context"
	"fmt"
	"net"
	"os"

	p "gitlab.com/imaginadio/golang/examples/grpc/protobuf"
	"google.golang.org/grpc"
)

type MsgServer struct {
	p.UnimplementedMsgServiceServer
}

func (MsgServer) GiveMeResp(ctx context.Context, req *p.Request) (*p.Response, error) {
	return &p.Response{
		Text:  req.Text + "000",
		Error: req.Subtext + "111",
	}, nil
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("usage: %s port_number", os.Args[0])
		return
	}

	port := ":" + os.Args[1]

	//ctx := context.Background()
	server := grpc.NewServer()
	var ms MsgServer
	p.RegisterMsgServiceServer(server, ms)
	l, err := net.Listen("tcp", port)
	if err != nil {
		panic(err)
	}
	server.Serve(l)
}
